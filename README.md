# Prometheus-ALB-Scraper
> A scraper for exposing AWS Application Load Balancers entries as HTTP healthcheck endpoints
## Table of Contents

- [Introduction](#introduction)
- [Running with Docker](#running-with-docker)
- [Running as service](#running-as-service)
- [License](#license)

## Introduction

The main goal of this scraper is to dynamically generate prometheus configurations to probe ALB endpoints with blackbox_exporter.

For each ALB, it will construct two yaml trees.
 
First one with external targets healthcheck endpoints when reachable (naming is ALB_<ALB NAME>.yaml). The generated yaml files will add the following labels to the vector metrics :
 - alb="<alb_name>"
 - check_type="instances" 
```
|_
   LISTENER
       |_
          Target Group 1
               |_external HTTP endpoint healthcheck
       |_
          Target Group 2
               |_external HTTP endpoint healthcheck
               ...
       |_
          Target Group n
               |_external HTTP endpoint healthcheck
```

Second one with internal targets healthcheck endpoints (naming is ALB_instances_<ALB NAME>.yaml). The generated yaml files will add the following labels to the vector metrics :
 - alb="<alb_name>"
 - check_type="ip" 
```
|_
   LISTENER
       |_
          Target Group 1
               |_HTTP endpoint 1 healthcheck
               |_HTTP endpoint 2 healthcheck
               ...
               |_HTTP endpoint n healthcheck 
       |_
          Target Group 2
               |_HTTP endpoint 1 healthcheck
               |_HTTP endpoint 2 healthcheck
               ...
               |_HTTP endpoint n healthcheck 
            ...
       |_
          Target Group n
               |_HTTP endpoint 1 healthcheck
               |_HTTP endpoint 2 healthcheck
               ...
               |_HTTP endpoint n healthcheck 
```

### Authenticating with AWS
#### IAM policy
```json
{
    "Version": "2012-10-17",
    "Statement": [
        {
            "Effect": "Allow",
            "Action": [
                "ec2:Describe*",
                "elasticloadbalancing:Describe*"
            ],
            "Resource": "*"
        }
    ]
}
```

#### Credentials
The library used for querying AWS APIs is Boto3. 

The easiest way is to set environment variables 
- AWS_DEFAULT_REGION
- AWS_ACCESS_KEY_ID
- AWS_SECRET_ACCESS_KEY

For full features, see https://boto3.amazonaws.com/v1/documentation/api/latest/guide/configuration.html

## Running with Docker

### Environment vars

All these environment variables can also be used running the Adhoc script (scraper.py) :

| Variable              | Description                                 | Default Value                   |
|-----------------------|---------------------------------------------|---------------------------------|
| SCRAPER_GEN_DIR       | Output Folder for generated scrapping files | ./generated                     |
| SCRAPER_TEMPLATE_DIR  | Template folder (file alb_check.tmpl is used for generating output files - uses Jinja2)         | .                               |
| SCRAPER_TMP_DIR       | Tmp dir                                     | ./tmp/generated/SCRAPER_TMP_DIR |
| SCRAPER_INTERNAL_DNS  | Internal DNS for AWS_TAG_Name dns resolving | example.com                     |
| REFRESH_PERIOD        | Interval for new scrapping (Docker only)    | 180                             |
| AWS_DEFAULT_REGION    | Default Region for AWS                      |                                 |
| AWS_ACCESS_KEY_ID     | Access key for AWS                          |                                 |
| AWS_SECRET_ACCESS_KEY | Secret access key for AWS                   |                                 |

### Docker run
```bash
docker run -e AWS_DEFAULT_REGION=eu-west-1 \
           -e AWS_SECRET_ACCESS_KEY=<FIXME> \
           -e AWS_ACCESS_KEY_ID=<FIXME> \
           -v /etc/prometheus/file_sd:/generated \
           <FIXME_IMAGE> 
```

## Running as service

The scraper can be run as a unitd service. Installation is done via Ansible role. See bellow for installation.

### Installing using Ansible

#### Ansible vars

| Variable                            | Description                                 | Default Value               |
|-------------------------------------|---------------------------------------------|-----------------------------|
| prometheus_alb_scraper_output_dir   | Output Folder for generated scrapping files | /etc/prometheus/file_sd     |
| prometheus_alb_scraper_dir          | Installation dir for scraping service       | /etc/prometheus-alb-scraper |
| prometheus_alb_scraper_internal_dns | Internal DNS for AWS_TAG_Name dns resolving | example.com                 |
| prometheus_alb_scraper_aws_region   | AWS Region                                  | eu-central-1                |
| prometheus_alb_scraper_service_name | Service Name                                | prometheus-alb-scraper      |
| prometheus_alb_scraper_period       | Interval for new scrapping                  | 180                         |

#### Ansible playbook

```yaml
- hosts: all
  roles:
    - act.prometheus-alb-scraper
  vars:
    prometheus_alb_scraper_internal_dns: "example.com"
    prometheus_alb_scraper_output_dir: "/etc/prometheus/file_sd"
    prometheus_alb_scraper_period: 180
```

## Using with prometheus + Blackbox exporter

The main goal of this scraper is to use it combined with the file_sd_config feature of prometheus

### prometheus.yml
```yaml
[...]
scrape_configs:
[...]
  - job_name: "prometheus-alb-servicediscovery"
    metrics_path: /probe
    params:
      module: [http_2xx]  # Look for a HTTP 200 response.
    file_sd_configs:
      - files:
          - "{{ prometheus_config_dir }}/file_sd/ALB_*.yaml"
        refresh_interval: 5m
    relabel_configs:
      - source_labels: [__address__]
        target_label: __param_target
      - source_labels: [__param_target]
        target_label: instance
      - target_label: __address__
        replacement: 127.0.0.1:9115  # The blackbox exporter's real hostname:port.
```

### rules.yml
```yaml
groups:
  - name: prometheus-alb-servicediscovery-alerts
    rules:
      - alert: app_down
        expr: 'probe_success{check_type="instances",job="prometheus-alb-servicediscovery"} != 1'
        for: 1m
        labels:
          severity: critical_prod
        annotations:
          identifier: "Global URL down {{ $labels.instance }}"
          description: "Global URL is down {{ $labels.instance }}. ALB : {{ $labels.alb }}"
      - alert: target_app_down
        expr: 'avg_over_time(probe_success{check_type="ip",job="prometheus-alb-servicediscovery"}[10m]) < 0.2'
        for: 1m
        labels:
          severity: critical_prod
        annotations:
          identifier: "Endpoint down {{ $labels.instance }}"
          description: "Endpoint is down {{ $labels.instance }}. ALB : {{ $labels.alb }}. More than 80% of health check missed for the last 10 minutes.Please check service status on server."
      - alert: SSL_soon_expire
      #2000000 ~~~~ 23 days
        expr: '(probe_ssl_earliest_cert_expiry{job="prometheus-alb-servicediscovery"}) - time() < 2000000'
        for: 5m
        labels:
          severity: critical
        annotations:
          identifier: "SSL Certificate will soon expire on instance {{ $labels.instance }}"
          description: "SSL Certificate will soon expire on instance {{ $labels.instance }} : {{ $labels.value }}"
```


### blackbox-exporter.yml

```yaml
modules:
  http_2xx:
    prober: http
    timeout: 5s
    http:
      method: GET
      no_follow_redirects: false
      fail_if_ssl: false
      fail_if_not_ssl: false
      tls_config:
        insecure_skip_verify: false
      preferred_ip_protocol: "ip4"
```

## License

- **[BSD 2-Clause License]**
- Copyright 2019 © Adrien CLEMENT.