FROM python:3.6-alpine

COPY /files/requirements.txt /

RUN pip install -r /requirements.txt

COPY files/ /
WORKDIR /
RUN chmod +x /docker_entrypoint.sh
RUN chmod +x /scraper.py

ENV REFRESH_PERIOD=180

CMD [ "/bin/sh", "-c", "/docker_entrypoint.sh" ]
