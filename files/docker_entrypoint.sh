#!/usr/bin/env sh
SLEEP_LOOP_SEC=${REFRESH_PERIOD}

log_message(){
echo '['`date +%Y-%m-%d" "%H:%M:%S.%N`'] '"$*"
}

###
while true
do
     log_message Scrapping ALB
     ./scraper.py
     sleep ${SLEEP_LOOP_SEC}
done
