#!/usr/bin/env python3
import boto3
import os
import shutil
from jinja2 import Environment, FileSystemLoader


def mkdir_ifn(dir):
    if os.path.exists(dir) & os.path.isdir(dir):
        print(dir + " already exists")
    elif os.path.isfile(dir):
        print(dir + " is a file")
        exit(1)
    else:
        os.makedirs(dir)


def rmfiles_in_dir(dir):
    for old_file in os.listdir(dir):
        file_path = os.path.join(dir, old_file)
        try:
            if os.path.isfile(file_path):
                os.unlink(file_path)
            # elif os.path.isdir(file_path): shutil.rmtree(file_path)
        except Exception as e:
            print(e)
            exit(1)


# empty list if instance is not running
def get_ec2_ips_from_id(param):
    return_list = []
    for reservation in instances['Reservations']:
        for instance in (item for item in reservation['Instances'] if (item['InstanceId'] == param and item['State']['Name'] == 'running')):
            for interface in instance['NetworkInterfaces']:
                return_list.append(interface['PrivateIpAddress'])
    return return_list


def get_ec2_name_from_id(param):
    for reservation in instances['Reservations']:
        for instance in (item for item in reservation['Instances'] if (item['InstanceId'] == param and item['State']['Name'] == 'running')):
            for tag in instance['Tags']:
                if tag['Key'] == 'Name':
                    return [tag['Value'] + '.' + INTERNAL_DNS]
    return get_ec2_ips_from_id(param)


OUTPUT_DIR = os.getenv("SCRAPER_GEN_DIR", './generated')
TEMPLATE_DIR = os.getenv("SCRAPER_TEMPLATE_DIR", '.')
TMP_DIR = os.getenv("SCRAPER_TMP_DIR", './tmp/generated/SCRAPER_TMP_DIR')
INTERNAL_DNS = os.getenv("SCRAPER_INTERNAL_DNS", 'example.com')

mkdir_ifn(OUTPUT_DIR)
mkdir_ifn(TMP_DIR)
rmfiles_in_dir(TMP_DIR)


client_elbv2 = boto3.client('elbv2')
client_ec2 = boto3.client('ec2')

instances = client_ec2.describe_instances()
target_groups = client_elbv2.describe_target_groups()['TargetGroups']
load_balancers = client_elbv2.describe_load_balancers()

for load_balancer in load_balancers['LoadBalancers']:
    healthcheck_list = []
    healthcheck_instance_list = []
    if load_balancer['Type'] == 'application':
        for listener in client_elbv2.describe_listeners(LoadBalancerArn=load_balancer['LoadBalancerArn'])['Listeners']:
            for rule in client_elbv2.describe_rules(ListenerArn=listener['ListenerArn'])['Rules']:
                if not rule['IsDefault']:
                    hostHeader = ''
                    pathPattern = ''
                    for condition in rule['Conditions']:
                        if condition['Field'] == 'host-header':
                            hostHeader = condition['HostHeaderConfig']['Values'][0]
                        if condition['Field'] == 'path-pattern':
                            pathPattern = condition['PathPatternConfig']['Values'][0].replace('*', '')
                    for action in rule['Actions']:
                        if action['Type'] == 'forward':
                            target_description = next(item for item in target_groups if item['TargetGroupArn'] == action['TargetGroupArn'] and (item['Protocol'] == 'HTTP' or item['Protocol'] == 'HTTPS'))
                            # check if healthcheck url is accessible from outside (AKA listener rule is not blocking healthck url)
                            if target_description['HealthCheckPath'].startswith(pathPattern):
                                healthcheck_list.append(listener['Protocol'].lower() + '://' + hostHeader + target_description['HealthCheckPath'])
                            for target_description_health in client_elbv2.describe_target_health(TargetGroupArn=action['TargetGroupArn'])['TargetHealthDescriptions']:
                                if target_description['TargetType'] == 'instance':
                                    for ip in get_ec2_name_from_id(target_description_health['Target']['Id']):
                                        healthcheck_instance_list.append(target_description['HealthCheckProtocol'].lower() + '://' + ip + ':' + target_description_health['HealthCheckPort'] + target_description['HealthCheckPath'])
                                elif target_description['TargetType'] == 'ip':
                                    healthcheck_instance_list.append(target_description['HealthCheckProtocol'].lower() + '://' + target_description_health['Target']['Id'] + ':' + target_description_health['HealthCheckPort'] + target_description['HealthCheckPath'])

        env = Environment(loader=FileSystemLoader(searchpath=TEMPLATE_DIR))
        alb_template = env.get_template('alb_check.tmpl')

        prometheus_lb_check_rendered = alb_template.render(
            target_health_check_array=healthcheck_list,
            alb_name=load_balancer['LoadBalancerName'],
            check_type="instances")
        with open(TMP_DIR+"/ALB_"+load_balancer['LoadBalancerName']+".yaml", "w") as fh:
            fh.write(prometheus_lb_check_rendered)
        prometheus_ip_check_rendered = alb_template.render(
            target_health_check_array=healthcheck_instance_list,
            alb_name=load_balancer['LoadBalancerName'],
            check_type="ip")
        with open(TMP_DIR+"/ALB_instances_"+load_balancer['LoadBalancerName']+".yaml", "w") as fh:
            fh.write(prometheus_ip_check_rendered)
    else:
        print(load_balancer['LoadBalancerArn'] + " is a " + load_balancer['Type'] + " LB - Not supported")

rmfiles_in_dir(OUTPUT_DIR)
src_files = os.listdir(TMP_DIR)
for file_name in src_files:
    full_file_name = os.path.join(TMP_DIR, file_name)
    if os.path.isfile(full_file_name):
        shutil.copy(full_file_name, OUTPUT_DIR)